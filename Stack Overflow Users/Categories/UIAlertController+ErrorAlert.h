//
//  UIAlertController+ErrorAlert.h
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/18/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIAlertController (ErrorAlert)

+ (void)displayError:(NSError *)error viewController:(UIViewController *)viewController actions:(NSArray<UIAlertAction *> *)actions;

@end

NS_ASSUME_NONNULL_END
