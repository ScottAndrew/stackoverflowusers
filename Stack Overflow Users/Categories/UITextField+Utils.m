//
//  UITextField+UITextField_Utils.m
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/18/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import "UITextField+Utils.h"

@implementation UITextField (Utils)

- (BOOL)isEmpty {
    return self.text.length == 0;
}

@end
