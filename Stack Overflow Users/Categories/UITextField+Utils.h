//
//  UITextField+Utils.h
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/18/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (UITextField_Utils)

@property (readonly) BOOL isEmpty;

@end

NS_ASSUME_NONNULL_END
