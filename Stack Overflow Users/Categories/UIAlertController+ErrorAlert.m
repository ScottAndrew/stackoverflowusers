//
//  UIAlertController+ErrorAlert.m
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/18/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import "UIAlertController+ErrorAlert.h"

@implementation UIAlertController (ErrorAlert)

+ (void)displayError:(NSError *)error viewController:(UIViewController *)viewController actions:(NSArray<UIAlertAction *> *)actions {
    if (error.localizedDescription == nil) {
        return;
    }

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:error.localizedDescription
                                                            preferredStyle: UIAlertControllerStyleAlert];

    if (actions != nil) {
        for (UIAlertAction *action in actions) {
            [alert addAction: action];
        }
    } else {
        [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:nil]];

    }

    [viewController presentViewController:alert animated:true completion:nil];
}

@end
