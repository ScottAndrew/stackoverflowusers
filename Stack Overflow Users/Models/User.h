//
//  User.h
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/17/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString* const UserBronzeBadgeKey;
extern NSString* const UserGoldBadgeKey;
extern NSString* const UserSilverBadgeKey;

@interface User : NSObject

@property(nonatomic, copy) NSString* name;
@property(nonatomic, assign) NSInteger reputation;
@property(nonatomic, copy) NSURL* profileImageURL;
@property(nonatomic, copy) NSDictionary<NSString *, NSNumber *> *badgeCounts;

-(NSNumber *) countForBadge:(NSString *)badgeType;
-(void)setBadgetCount:(NSInteger)count for:(NSString*)badge;

@end

NS_ASSUME_NONNULL_END
