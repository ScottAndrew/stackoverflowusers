//
//  User.m
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/17/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import "User.h"

@implementation User

NSString* const UserBronzeBadgeKey = @"bronze";
NSString* const UserGoldBadgeKey = @"gold";
NSString* const UserSilverBadgeKey = @"silver";

- (void)setValuesForKeysWithDictionary:(NSDictionary<NSString *,id> *)keyedValues {
    // We won't call super but we will do some work here.
    self.name = keyedValues[@"display_name"];
    self.reputation = [keyedValues[@"reputation"] intValue];
    self.profileImageURL = keyedValues[@"profile_image"];
    self.badgeCounts = keyedValues[@"badge_counts"];
}

- (NSNumber *)countForBadge:(NSString *)badgeType {
    if (self.badgeCounts[badgeType] != nil) {
        return self.badgeCounts[badgeType];
    } else {
        return [NSNumber numberWithInteger:0];
    }
}

-(void)setBadgetCount:(NSInteger)count for:(NSString*)badge {
    NSMutableDictionary *mutableDictionary = [NSMutableDictionary new];

    if (self.badgeCounts != nil) {
        [mutableDictionary setValuesForKeysWithDictionary:self.badgeCounts];
    }

    mutableDictionary[badge] = [NSNumber numberWithInteger:count];

    self.badgeCounts = mutableDictionary.copy;
}

@end
