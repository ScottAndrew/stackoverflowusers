//
//  StackoverflowRunner.h
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/17/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//
#import <Foundation/Foundation.h>

@protocol StackOverflowAction;

typedef void (^StackOverflowRunnerCallback)(NSError*);

@interface StackOverflowRunner: NSObject
-(void)run:(id<StackOverflowAction>)api responseHandler:(StackOverflowRunnerCallback)callback;
@end
