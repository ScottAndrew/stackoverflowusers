//
//  StackOverflowAPI.h
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/17/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

@import Foundation;

@protocol StackOverflowAction <NSObject>

@property (readonly, copy) NSString *apiPath;
- (NSError *)processData:(NSData*)data;
@end


