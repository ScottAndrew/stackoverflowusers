//
//  StackOverflowAPI.m
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/17/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.

#import "StackOverflowAPI.h"
#import "GetUsersAction.h"
#import "StackOverFlowAction.h"
#import "StackOverflowRunner.h"
@implementation StackOverflowAPI

+ (void)getUsers:(void(^)(NSArray<User *> *, NSError*))callback {
    GetUsersAction *action = [GetUsersAction new];
    StackOverflowRunner *runner = [StackOverflowRunner new];
    
    [runner run:action responseHandler:^(NSError *error) {
        // we need to return this back one more level..
        callback(action.users, error);
    }];
}

@end


