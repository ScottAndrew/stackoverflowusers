//
//  GetUsersAction.h
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/17/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StackOverflowAction.h"

NS_ASSUME_NONNULL_BEGIN

@class User;

@interface GetUsersAction : NSObject<StackOverflowAction>

@property (readonly, copy) NSArray<User *>* users;

@end

NS_ASSUME_NONNULL_END
