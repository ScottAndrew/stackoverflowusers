//
//  GetUsersAction.m
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/17/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import "GetUsersAction.h"
#import "User.h"

@interface GetUsersAction()
@property (readwrite, copy) NSArray<User *>* users;
@end

@implementation GetUsersAction
- (NSString *)apiPath {
    return @"users?site=stackoverflow";
}

- (NSError *)processData:(NSData *)data {
    NSError* error = nil;
    NSDictionary *object = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    
    if (error == nil && object[@"items"] != nil) {
        NSMutableArray<User *> *userList = [NSMutableArray new];
        
        // Lets add each found user in items.
        for (NSDictionary *item in object[@"items"]) {
            User* user = [User new];
            [user setValuesForKeysWithDictionary: item];
            
            [userList addObject:user];
        }
        
        self.users = userList;
    }
    
    return error;
}

@end
