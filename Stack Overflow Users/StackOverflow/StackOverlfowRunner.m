//
//  StackOverlfowRunner.m
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/17/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//


#import "StackOverflowRunner.h"
#import "StackOverflowAction.h"

#define BaseAddress @"https://api.stackexchange.com/2.2/"
#define HTTP_OK 200

@interface StackOverflowRunner()
@property (nonatomic, copy) NSURL *baseURL;

-(NSError*) processCode:(NSInteger)code;
@end

@implementation StackOverflowRunner

-(instancetype)init {
    self.baseURL = [NSURL URLWithString:BaseAddress];
    
    return [super init];
}

-(void) run:(id<StackOverflowAction>)api responseHandler:(StackOverflowRunnerCallback)callback {
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *apiURL = [NSURL URLWithString:api.apiPath relativeToURL:self.baseURL];
    NSURLSessionTask* task = [session dataTaskWithURL:apiURL
                                    completionHandler:^(NSData *data,
                                                        NSURLResponse *response,
                                                        NSError *error)
    {
       NSError *operationError = error;
        
       if (operationError == nil) {
           operationError = [self processCode:((NSHTTPURLResponse*)response).statusCode];
       }
        
        // if we still have no error we can process our data.
        if (operationError == nil) {
            operationError = [api processData:data];
        }
        
        callback(operationError);
    }];
    
    [task resume];
}

- (NSError*)processCode:(NSInteger)code {
    // We will be simple and consider 200 ok. Any other
    // is an error we can return.
    if (code == 200) {
        return nil;
    } else {
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey: @"Network operation was uncessful."};
        return [NSError errorWithDomain:NSURLErrorDomain
                                   code:code
                               userInfo:userInfo];
    }
}

@end
