//
//  AddUserViewController.m
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/18/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import "AddUserViewController.h"
#import "NumberField.h"
#import "UITextField+Utils.h"
#import "User.h"

@interface AddUserViewController()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet NumberField *reputationField;
@property (weak, nonatomic) IBOutlet NumberField *goldBadgesField;
@property (weak, nonatomic) IBOutlet NumberField *silverBadgesField;
@property (weak, nonatomic) IBOutlet NumberField *bronzeBadgesField;
@property (weak, nonatomic) IBOutlet UILabel *userErrorField;
@property (weak, nonatomic) IBOutlet UILabel *reputationErrorField;
@property (weak, nonatomic) IBOutlet UILabel *goldBadgeErrorField;
@property (weak, nonatomic) IBOutlet UILabel *silverBadgeErrorField;
@property (weak, nonatomic) IBOutlet UILabel *bronzeBadgeErrorField;


@property (weak, nonatomic) IBOutlet UIButton *addUserButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addUserButtonBottomConstraint;

@end

@implementation AddUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNumberFields];

    [self enableAddUserButton:self.addUserButton.isEnabled];

    // Keyboard..
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onKeyboardWillShowNotification:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];

    [self.userNameField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)onValueChanged:(id)sender {
    BOOL enableButton = !self.userNameField.isEmpty &&
                        !self.reputationField.isEmpty &&
                        !self.goldBadgesField.isEmpty &&
                        !self.silverBadgesField.isEmpty &&
                        !self.bronzeBadgesField.isEmpty;

    [self enableAddUserButton:enableButton];
}

-(IBAction)onAddUser:(id)sender {
    // Button was clicked we just need validate numbers.
    if ([self validate]) {
        User *user = [User new];

        user.name = self.userNameField.text;
        user.reputation = self.reputationField.value;

        [user setBadgetCount:self.bronzeBadgesField.value for:UserBronzeBadgeKey];
        [user setBadgetCount:self.silverBadgesField.value for:UserSilverBadgeKey];
        [user setBadgetCount:self.goldBadgesField.value for:UserGoldBadgeKey];

        if (self.userAdded != nil) {
            self.userAdded(user);
        }

        [self.navigationController popViewControllerAnimated:true];
    }
}

- (void)onKeyboardWillShowNotification:(NSNotification *)notification {
     CGRect endRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];

    // We always have a keyboard so we don't need to animate. We do need to take
    // safe area/layout margin into account.
    if (@available(ios 11, *)) {
        self.addUserButtonBottomConstraint.constant = -(CGRectGetHeight(endRect) - self.view.safeAreaInsets.bottom) ;
    } else {
        self.addUserButtonBottomConstraint.constant = -(CGRectGetHeight(endRect) - self.bottomLayoutGuide.length);
    }
}

#pragma mark - UITextFieldDelegate handlers
- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    if (textField == self.userNameField) {
        [self.reputationField becomeFirstResponder];
    } else if (textField == self.reputationField)  {
        [self.goldBadgesField becomeFirstResponder];
    } else if (textField == self.goldBadgesField)  {
        [self.silverBadgesField becomeFirstResponder];
    } else if (textField == self.silverBadgesField)  {
        [self.bronzeBadgesField becomeFirstResponder];
    } else if (textField == self.bronzeBadgesField) {
        [self onAddUser:self];
    }

    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    // Use the number filtering in our number field.
    if ([textField isKindOfClass:[NumberField class]]) {
        return [((NumberField*)textField) canAcceptStringForInput:string];
    }

    return true;
}

-(void)setupNumberFields {
    self.reputationField.validationRule = NumberFieldValidationRuleOdd;
    self.goldBadgesField.validationRule = NumberFieldValidationRuleDivisbleBy3;
    self.silverBadgesField.validationRule = NumberFieldValidationRuleDivisbleBy3;
    self.bronzeBadgesField.validationRule = NumberFieldValidationRuleDivisbleBy3;
}

- (BOOL)validate {

    UIView * viewToBecomeResponder = nil;

    // Crawl backward through the views validating tbe last one
    // not valid will become the first responder.
    if (!self.bronzeBadgesField.isValid) {
        self.bronzeBadgeErrorField.hidden = false;
        viewToBecomeResponder = self.bronzeBadgesField;
    } else {
        self.bronzeBadgeErrorField.hidden = true;
    }

    if (!self.silverBadgesField.isValid) {
        self.silverBadgeErrorField.hidden = false;
        viewToBecomeResponder = self.silverBadgesField;
    } else {
        self.silverBadgeErrorField.hidden = true;
    }

    if (!self.goldBadgesField.isValid) {
        self.goldBadgeErrorField.hidden = false;
        viewToBecomeResponder = self.goldBadgesField;
    } else {
        self.goldBadgeErrorField.hidden = true;
    }

    if (!self.reputationField.isValid) {
        self.reputationErrorField.hidden = false;
        viewToBecomeResponder = self.reputationField;
    } else {
        self.reputationErrorField.hidden = true;
    }

    if (self.userNameField.isEmpty) {
        self.userErrorField.hidden = false;
        viewToBecomeResponder = self.userNameField;
    } else {
        self.userErrorField.hidden = true;
    }

    if (viewToBecomeResponder != nil) {
        [viewToBecomeResponder becomeFirstResponder];
    }

    return viewToBecomeResponder == nil;
}

- (void)enableAddUserButton:(BOOL)enabled {
    self.addUserButton.enabled = enabled;
    self.addUserButton.alpha = enabled ? 1.0 : 0.5;
}

@end
