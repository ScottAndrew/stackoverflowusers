//
//  NumberField.h
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/18/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, NumberFieldValidationRule) {
    NumberFieldValidationRuleNone,
    NumberFieldValidationRuleDivisbleBy3,
    NumberFieldValidationRuleOdd
};

@interface NumberField : UITextField

@property (assign, nonatomic) NumberFieldValidationRule validationRule;
@property (assign, readonly) BOOL isValid;
@property (assign, readonly) NSUInteger value;

// Instead of hijaking delegate we will supply a helper..
- (BOOL)canAcceptStringForInput:(NSString *)string;

@end

NS_ASSUME_NONNULL_END
