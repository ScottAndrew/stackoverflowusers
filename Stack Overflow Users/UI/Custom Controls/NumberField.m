//
//  NumberField.m
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/18/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import "NumberField.h"
#import "UITextField+Utils.h"

@implementation NumberField

- (void)awakeFromNib {
    [super awakeFromNib];

    [self createAcessoryView];

    self.validationRule = NumberFieldValidationRuleNone;
}

#pragma mark - UITextFieldDelegateHandling

- (BOOL)canAcceptStringForInput:(NSString *)string {
    if (string.length == 0) {
        return YES;
    }

    if ([string rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound) {
        return YES;
    }

    return NO;
}

- (void)doneWithNumberPad {
    [self.delegate textFieldShouldReturn:self];
}

- (void)createAcessoryView {

    NSString *buttonText = self.returnKeyType == UIReturnKeyDone ? @"Done" : @"Next";
    UIBarButtonItemStyle style = self.returnKeyType == UIReturnKeyDone ? UIBarButtonItemStyleDone : UIBarButtonItemStylePlain;

    UIToolbar *numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:buttonText style:style target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];

    self.inputAccessoryView = numberToolbar;
}

- (BOOL)isValid {

    if (self.text == nil || self.isEmpty) {
        return false;
    }

    switch (self.validationRule) {
        case NumberFieldValidationRuleDivisbleBy3:
            return self.value % 3 == 0;

        case NumberFieldValidationRuleOdd:
            return self.value % 2 != 0 && self.value != 0;

        default:
            return true;
    }
}

- (NSUInteger)value {
    return self.text.integerValue;
}

@end
