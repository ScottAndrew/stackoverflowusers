//
//  UserTableViewCell.h
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/17/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class User;

@interface UserTableViewCell : UITableViewCell

@property (nonatomic, assign, nullable) User* user;

+ (NSString *)reusableCellID;

@end

NS_ASSUME_NONNULL_END
