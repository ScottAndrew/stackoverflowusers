//
//  UserTableViewCell.m
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/17/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import "SDWebImage/SDWebImage.h"
#import "UserTableViewCell.h"
#import "User.h"

@interface UserTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *reputationLabel;
@property (weak, nonatomic) IBOutlet UILabel *goldBadgeCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *silverBadgeCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *bronzeBadgeCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@end

@implementation UserTableViewCell

+ (NSNumberFormatter*)numberFormatter {
    static NSNumberFormatter* numberFormatter;
    
    if (numberFormatter == nil) {
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        formatter.usesGroupingSeparator = YES;
        
        numberFormatter = formatter;
    }
    
    return numberFormatter;
}

+ (NSString *) reusableCellID {
    return @"UserTableViewCell";
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    [self.avatarImageView sd_cancelCurrentImageLoad];
    
    self.user = nil;
}

- (void)setUser:(User *)user {
    _user = user;
    
    if (user != nil) {
        NSNumberFormatter* formatter = [UserTableViewCell numberFormatter];
        
        self.userNameLabel.text = user.name;
        self.reputationLabel.text = [formatter
                                     stringFromNumber:[NSNumber numberWithInteger:user.reputation]];
        
        self.goldBadgeCountLabel.text = [user countForBadge: UserGoldBadgeKey].stringValue;
        self.silverBadgeCountLabel.text = [user countForBadge: UserSilverBadgeKey].stringValue;
        self.bronzeBadgeCountLabel.text = [user countForBadge: UserBronzeBadgeKey].stringValue;
        
        [self.avatarImageView sd_setImageWithURL:user.profileImageURL];
    }
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
