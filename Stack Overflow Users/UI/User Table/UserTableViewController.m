//
//  UserTableViewController.m
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/17/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import "UserTableViewController.h"
#import "StackOverflowAPI.h"
#import "User.h"
#import "UserTableViewCell.h"
#import "AddUserViewController.h"
#import "UIAlertController+ErrorAlert.h"

@interface UserTableViewController ()
@property (readwrite, nonatomic, copy) NSArray *users;
@property (readwrite, nonatomic) UIActivityIndicatorView *activityIndicator;
@end

@implementation UserTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.users = [NSArray new];

    [self showActivityIndicator];

    [self loadUsers];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.users.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserTableViewCell *cell = [tableView
                               dequeueReusableCellWithIdentifier:[UserTableViewCell reusableCellID]
                               forIndexPath:indexPath];
    
    if (cell != nil) {
        cell.user = self.users[indexPath.row];
    }
    
    return cell;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    self.tableView.scrollEnabled = self.users.count > 0;
}

#pragma mark - UI Helpers
- (void)showActivityIndicator {
    // If we aren't showing the indicator already create, animate and center.
    if (self.activityIndicator == nil) {
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

        self.activityIndicator.hidden = false;
        [self.tableView addSubview:self.activityIndicator];
        [self.tableView bringSubviewToFront:self.activityIndicator];
        [self.activityIndicator startAnimating];

        self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
        [NSLayoutConstraint activateConstraints: @[[self.activityIndicator.centerXAnchor constraintEqualToAnchor:self.tableView.centerXAnchor],
                                                  [self.activityIndicator.centerYAnchor constraintEqualToAnchor:self.tableView.centerYAnchor]]];
    }
}

- (void)removeActivityIndicator {
    if (self.activityIndicator != nil) {
        [self.activityIndicator removeFromSuperview];
        self.activityIndicator = nil;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[AddUserViewController class]]) {
        AddUserViewController* addUserController = (AddUserViewController *)segue.destinationViewController;

        __weak UserTableViewController *weakSelf = self;

        addUserController.userAdded = ^(User *user) {
            NSMutableArray *updatedList = [weakSelf.users mutableCopy];
            [updatedList addObject:user];

            weakSelf.users = updatedList;

            NSArray<NSIndexPath *>* indexPaths = @[[NSIndexPath indexPathForRow:updatedList.count -1 inSection:0]];
            [weakSelf.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
            [weakSelf.tableView scrollToRowAtIndexPath:indexPaths[0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        };
    }
}

#pragma mark -Data loading
- (void)loadUsers {
    __weak UserTableViewController *weakSelf = self;

    [StackOverflowAPI getUsers:^(NSArray<User *> *users, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            if (error != nil) {
                UIAlertAction *retryAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    dispatch_async(dispatch_get_main_queue(), ^ {
                        [weakSelf loadUsers];
                    });
                }];

                [UIAlertController displayError:error viewController:weakSelf actions:@[retryAction]];

            } else {
                weakSelf.users = users;
                [weakSelf removeActivityIndicator];
                [weakSelf.tableView reloadData];
            }
        });
    }];
}
@end
