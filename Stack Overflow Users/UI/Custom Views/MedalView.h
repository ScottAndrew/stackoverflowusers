//
//  MedialView.h
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/17/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
IB_DESIGNABLE
@interface MedalView : UIView

@property(retain, nonatomic) IBInspectable UIColor* medalColor;

@end

NS_ASSUME_NONNULL_END
