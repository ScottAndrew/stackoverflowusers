//
//  MedialView.m
//  Stack Overflow Users
//
//  Created by Scott Andrew on 4/17/19.
//  Copyright © 2019 Scott Andrew. All rights reserved.
//

#import "MedalView.h"

@interface MedalView()
@property (readwrite, retain) CAShapeLayer* circle;
@end

@implementation MedalView

+(Class)layerClass {
    return [CAShapeLayer class];
}

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
   
    return self;
}

-(instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    return self;
}

-(void)setMedalColor:(UIColor *)medalColor {
    _medalColor = medalColor;
    
    CAShapeLayer *ourLayer = (CAShapeLayer *)self.layer;
    ourLayer.fillColor = medalColor.CGColor;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    CAShapeLayer *ourLayer = (CAShapeLayer *)self.layer;
    ourLayer.path = [UIBezierPath bezierPathWithOvalInRect:self.bounds].CGPath;
}

@end
